<?php

namespace App\Repository;

use Doctrine\ORM\Query;
use App\Entity\Videomp;

class Videopdo
{
  public function getAll() : array
  {
    $videos = [];
    try {
      $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
      //new \PDO("mysql:host=localhost:3306; dbname=mabdd", "user", "mdp");(modifié)
      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

      $query = $cnx->prepare("SELECT * FROM Video");
      $query->execute();

     // dump($query->fetchAll());

      foreach ($query->fetchAll() as $row) {
        $video = new Videomp();
        $video->fromSQL($row);
        $videos[] = $video;
      }

    } catch (\PDOExeption $e) {
      dump($e);
    }
    return $videos;
  }

  public function add(Videomp $video)
  {
    try {
      $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
      //new \PDO("mysql:host=localhost:3306; dbname=mabdd", "user", "mdp");(modifié)
      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

      $query = $cnx->prepare("INSERT INTO db.Video(author,title,link,date,hidden,tag,category) VALUES (:author,:title,:link,:date,:hidden,:tag,:category)");

      $query->bindValue(":author", $video->author);
      $query->bindValue(":title", $video->title);
      $query->bindValue(":link", $video->link);
      $query->bindValue(":date", $video->date->format('Y-m-d'));
      $query->bindValue(":hidden", $video->hidden);
      $query->bindValue(":tag", $video->tag);
      $query->bindValue(":category", $video->category);

      $query->execute();

      $video->id = intval($cnx->lastInsertId());
    } catch (\PDOExeption $e) {
      dump($e);
    }
  }
  public function sup(int $id)
  {
    try {
      $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
    //new \PDO("mysql:host=localhost:3306; dbname=mabdd", "user", "mdp");(modifié)
      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

      $query = $cnx->prepare("DELETE FROM Video
    WHERE id=:id");
      $query->bindValue(":id", $id);
      $query->execute();


    } catch (\PDOExeption $e) {
      dump($e);
    }
  }

  public function getById(int $id)
  {
    try {
      $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);


      $query = $cnx->prepare("SELECT * FROM Video WHERE id = :id");

      $query->bindValue(":id", $id);

      $query->execute();

      $result = $query->fetchAll();

      if (count($result) === 1) {
        $video = new Videomp();
        $video->fromSQL($result[0]);
        return $video;
      }

      $video->id = intval($cnx->lastInsertId());

    } catch (\PDOException $e) {
      dump($e);
    }
  }

  public function modif(Videomp $video)
  {
    try {

      $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
    //new \PDO("mysql:host=localhost:3306; dbname=mabdd", "user", "mdp");(modifié)
      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

      $query = $cnx->prepare("UPDATE Video SET author = :author,title = :title,link = :link,date = :date,hidden = :hidden,tag = :tag ,category = :category WHERE id = :id");

      $query->bindValue(":id", $video->id);
      $query->bindValue(":author", $video->author);
      $query->bindValue(":title", $video->title);
      $query->bindValue(":link", $video->link);
      $query->bindValue(":date", $video->date->format('Y-m-d'));
      $query->bindValue(":hidden", $video->hidden);
      $query->bindValue(":tag", $video->tag);
      $query->bindValue(":category", $video->category);

      $query->execute();

      $video->id = intval($cnx->lastInsertId());
    } catch (\PDOExeption $e) {
      dump($e);
    }
  }
}