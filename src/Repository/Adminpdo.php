<?php

namespace App\Repository;


use App\Entity\Admin;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class Adminpdo implements UserProviderInterface
{
  private $cnx;

  public function __construct()
  {
    try {
      $this->cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
      //new \PDO("mysql:host=localhost:3306; dbname=mabdd", "user", "mdp");(modifié)
      $this->cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

    } catch (\PDOExeption $e) {
      dump($e);
    }
  }

  private function fetch(string $query, array $params = [])
  {
    try {
      $query = $this->cnx->prepare($query);

      foreach ($params as $param => $value) {
        $query->bindValue($param, $value);


      }

      $query->execute();

      $result = [];

      foreach ($query->fetchAll() as $row) {
        $result[] = Admin::fromSQL($row);
      }

      if (count($result) <= 1) {
        return $result[0];
      }
      return $result;

    } catch (\PDOException $e) {
      dump( $e);
    }

  }

  public function add(User $user)
  {
    $this->fetch("INSERT INTO Admin ( email, password) VALUES (:email, :password)", [
      ":email" => $user->email,
      ":password" => $user->password
    ]);
    $user->id = intval($this->connection->lastInsertId());
        // ... puis on retourne l'utilisateur
    return $user;

  }

  public function get(int $id)
  {
      // on appelle la méthode privée `fetch()` pour selectionner notre utilisateur.
    return $this->fetch("SELECT * FROM user WHERE :id", [":id" => $id]);
  }

  /**
   * La méthode `getAll()` permet de sélectionner tout les utilisateurs dans la basede données.
   * 
   * @param int $id L'id de l'utilisateur à selectionner.
   * @return array|User|null L'utilisateur correspondant à l'id.
   */
  public function getAll()
  {
      // on appelle la méthode privée `fetch()` pour selectionner tout les utilisateurs.
    return $this->fetch("SELECT * FROM Admin");
  }


  public function loadUserByUsername($username)
  {
    $user = $this->fetch("SELECT * FROM Admin WHERE mail=:email", [
      ":email" => $username
    ]);
    if ($user instanceof Admin) {
      return $user;
    }
    throw new UsernameNotFoundException("User doesn't exist");
  }

  public function refreshUser(\Symfony\Component\Security\Core\User\UserInterface $user)
  {
    return $this->loadUserByUsername($user->getUsername());
  }

  public function supportsClass($class)
  {
    return Admin::class === $class;
  }
}

  
  