<?php

namespace App\Repository;

use Doctrine\ORM\Query;
use App\Entity\Article;

class Articlepdo
{

  public function getAll() : array
  {
    $articles = [];
    try {
      $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
      //new \PDO("mysql:host=localhost:3306; dbname=mabdd", "user", "mdp");(modifié)
      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

      $query = $cnx->prepare("SELECT * FROM Article");
      $query->execute();

     // dump($query->fetchAll());

      foreach ($query->fetchAll() as $row) {
        $article = new Article();
        $article->fromSQL($row);
        $articles[] = $article;
      }

    } catch (\PDOExeption $e) {
      dump($e);
    }
    return $articles;
  }

  public function add(Article $article)
  {
    try {
      $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
      //new \PDO("mysql:host=localhost:3306; dbname=mabdd", "user", "mdp");(modifié)
      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

      $query = $cnx->prepare("INSERT INTO db.Article(author,title,header,main,date,hidden,tag,category) VALUES (:author,:title,:header,:main,:date,:hidden,:tag,:category)");

      $query->bindValue(":author", $article->author);
      $query->bindValue(":title", $article->title);
      $query->bindValue(":header", $article->header);
      $query->bindValue(":main", $article->main);
      $query->bindValue(":date", $article->date->format('Y-m-d'));
      $query->bindValue(":hidden", $article->hidden);
      $query->bindValue(":tag", $article->tag);
      $query->bindValue(":category", $article->category);

      $query->execute();

      $article->id = intval($cnx->lastInsertId());
    } catch (\PDOExeption $e) {
      dump($e);
    }
  }

  public function sup(int $id)
  {
    try {
      $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
      //new \PDO("mysql:host=localhost:3306; dbname=mabdd", "user", "mdp");(modifié)
      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

      $query = $cnx->prepare("DELETE FROM Article
      WHERE id=:id");
      $query->bindValue(":id", $id);
      $query->execute();


    } catch (\PDOExeption $e) {
      dump($e);
    }
  }

  public function modif(Article $article)
  {
    try {

      $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
    //new \PDO("mysql:host=localhost:3306; dbname=mabdd", "user", "mdp");(modifié)
      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

      $query = $cnx->prepare("UPDATE Article SET author = :author,title = :title,header = :header,main = :main,date = :date,hidden = :hidden,tag = :tag ,category = :category WHERE id = :id");

      $query->bindValue(":id", $article->id);
      $query->bindValue(":author", $article->author);
      $query->bindValue(":title", $article->title);
      $query->bindValue(":header", $article->header);
      $query->bindValue(":main", $article->main);
      $query->bindValue(":date", $article->date->format('Y-m-d'));
      $query->bindValue(":hidden", $article->hidden);
      $query->bindValue(":tag", $article->tag);
      $query->bindValue(":category", $article->category);

      $query->execute();

      $article->id = intval($cnx->lastInsertId());
    } catch (\PDOExeption $e) {
      dump($e);
    }
  }

  public function getById(int $id)
  {
    try {
      $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);


      $query = $cnx->prepare("SELECT * FROM Article WHERE id = :id");

      $query->bindValue(":id", $id);

      $query->execute();

      $result = $query->fetchAll();

      if (count($result) === 1) {
        $article = new Article();
        $article->fromSQL($result[0]);
        return $article;
      }

      $article->id = intval($cnx->lastInsertId());

    } catch (\PDOException $e) {
      dump($e);
    }
  }
}

