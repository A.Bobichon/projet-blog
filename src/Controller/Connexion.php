<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\UserRepository;
use App\Entity\Admin;

class Connexion extends AbstractController
{

  /**
   * @Route("/connexion", name="connexion")
   * @Security("has_role('ROLE_USER')")
   */
  public function index()
  {
    return $this->redirectToRoute('gestion');
  }

}