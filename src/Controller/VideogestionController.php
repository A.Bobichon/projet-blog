<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Videomp;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use App\Repository\Videopdo;

class VideogestionController extends Controller
{
    /**
     * @Route("/videogestion", name="videogestion")
     */
    public function createVideo(Request $request, Videopdo $pdo)
    {
        $video = new Videomp();

        $form = $this->createFormBuilder($video)
            ->add("author", TextType::class)
            ->add("title", TextType::class)
            ->add("link", TextType::class)
            ->add("date", DateType::class)
            ->add("hidden", HiddenType::class)
            ->add("tag", TextType::class)
            ->add("category", TextType::class)
            ->add("save", SubmitType::class, array('label' => 'Create Video'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
              

            $video = $form->getData();
 
            $pdo->add($video);

            return $this->redirectToRoute('gestion');
        }
        return $this->render('videogestion.html.twig', [
            'controller_name' => 'VideogestionController',
            'form' => $form->createView(),
            'video' => $video,

        ]);


    }

}
