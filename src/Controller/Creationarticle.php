<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Article;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use App\Repository\Articlepdo;


class Creationarticle extends AbstractController
{

  /**
   * @Route("/creationarticle", name="creationarticle")
   */

  public function createArticle(Request $request, Articlepdo $pdo)
  {
    $article = new Article();

    $form = $this->createFormBuilder($article)
      ->add("author", TextType::class)
      ->add("title", TextType::class)
      ->add("header", TextareaType::class)
      ->add("main", TextareaType::class)
      ->add("date", DateType::class)
      ->add("tag", TextType::class)
      ->add("category", TextType::class)
      ->add("hidden", HiddenType::class)
      ->add('save', SubmitType::class, array('label' => 'Create Article'))
      ->getForm();

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
                      
        $article = $form->getData();
        
        $pdo->add($article);
                   
        return $this->redirectToRoute('gestion');
      }


    return $this->render("creationarticle.html.twig", [
      "form" => $form->createView(),
      "article" => $article
    ]);



    
  }

  /**
   * @Route("inscription_show", name="inscription_show")
   */

  public function show(){

    return $this->render('creationarticle.html.twig',
    []);
  }

}