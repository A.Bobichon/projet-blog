<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\Videopdo;

class Video extends AbstractController
{

  /**
   * @Route("/video", name="video")
   */

  public function index(Videopdo $repo)
  {

    $video = $repo->getAll();

    return $this->render("video.html.twig", [
      'video' => $video,
      ]);
  }

}
