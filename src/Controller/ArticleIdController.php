<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\Articlepdo;
use App\Entity\Article;

class ArticleIdController extends Controller
{
    /**
     * @Route("/article/id/{id}", name="article_id")
     */
    public function index(int $id, Articlepdo $repo)
    {

        $article = $repo->getById($id);

        return $this->render('article_id/index.html.twig', [
            'controller_name' => 'ArticleIdController',
            'article' => $article
        ]);
    }
}
