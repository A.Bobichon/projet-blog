<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use App\Repository\Videopdo;
use App\Entity\Videomp;
use Symfony\Component\HttpFoundation\Request;

class ModifVideoController extends Controller
{
    /**
     * @Route("/modif/video/{id}", name="modif_video")
     */
    public function modifvideo(Request $request, Videopdo $pdo, int $id)
    {

        $video = $pdo->getById($id);
        $form = $this->createFormBuilder($video)
            ->add("author", TextType::class)
            ->add("title", TextType::class)
            ->add("link", TextareaType::class)
            ->add("date", DateType::class)
            ->add("tag", TextType::class)
            ->add("category", TextType::class)
            ->add("hidden", HiddenType::class)
            ->add('save', SubmitType::class, array('label' => 'modif video'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $video = $form->getData();

            $pdo->modif($video);

            return $this->redirectToRoute('gestion');
        }
        return $this->render('modif_video/index.html.twig', [
            'controller_name' => 'ModifVideoController',
            "form" => $form->createView(),
            "video" => $video
        ]);

    }
}
