<?php

namespace App\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\Videopdo;
use App\Entity\Videomp;
use Symfony\Component\HttpFoundation\Request;

class SuprimervideoController extends Controller
{
    /**
     *  @Route("/supprimervideo/{id}", name="supprimervideo")
     */
    public function suparticle(Videopdo $repo, request $request, int $id)
    {

        $video = $repo->sup($id);

        return $this->redirectToRoute('gestion');
    }
}
