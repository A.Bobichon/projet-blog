<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Article;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use App\Repository\Articlepdo;


class ModifierarticleController extends Controller
{
    /**
     * @Route("/modifierarticle/{id}", name="modifierarticle")
     */
    public function modifArticle(Request $request, Articlepdo $pdo, int $id)
    {
        $article = $pdo->getById($id);
        $form = $this->createFormBuilder($article)
            ->add("author", TextType::class)
            ->add("title", TextType::class)
            ->add("header", TextareaType::class)
            ->add("main", TextareaType::class)
            ->add("date", DateType::class)
            ->add("tag", TextType::class)
            ->add("category", TextType::class)
            ->add("hidden", HiddenType::class)
            ->add('save', SubmitType::class, array('label' => 'modif article'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $article = $form->getData();

            $pdo->modif($article);

            return $this->redirectToRoute('gestion');
        }

        return $this->render('modifierarticle/index.html.twig', [
            "form" => $form->createView(),
            "article" => $article
        ]);

    }
}
