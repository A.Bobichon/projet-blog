<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\Articlepdo;
use App\Entity\Article;
use Symfony\Component\HttpFoundation\Request;

class SupprimerarticleController extends Controller
{
    /**
     * @Route("/supprimerarticle/{id}", name="supprimerarticle")
     */
    public function suparticle(Articlepdo $repo, request $request, int $id)
    {   
        $article = $repo->sup($id);

        return $this->redirectToRoute('gestion');
    }
}