<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\Articlepdo;
use App\Repository\Videopdo;


class Gestion extends AbstractController
{

  /**
   * @Route("/gestion", name="gestion")
   */

  public function index(Articlepdo $repo, Videopdo $videorepo)
  {

    $article = $repo->getAll();
    $video = $videorepo->getAll();

    return $this->render("gestion.html.twig", [
      'article' => $article, 
      'video' => $video
      ]);
  }
}
