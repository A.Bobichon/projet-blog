<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Admin;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Repository\Adminpdo;

class Inscription extends AbstractController
{

  /**
   * @Route("/inscription", name="inscription")
   */
  public function create(Request $request, Adminpdo $pdo)
  {
    $admin = new Admin();

    $form = $this->createFormBuilder($admin)
      ->add("nom", TextType::class)
      ->add("prenom", TextType::class)
      ->add("pseudo", TextType::class)
      ->add("mail", TextType::class)
      ->add("password", TextType::class)
      ->add('save', SubmitType::class, array('label' => 'Create Admin'))
      ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
                     
      $admin = $form->getData();

      $pdo->add($admin);

      return $this->redirectToRoute('affiche');
    }

    return $this->render("inscription.html.twig", [
      "form" => $form->createView()
    ]);
    

    
  }

  /**
   * @Route("inscription_show", name="inscription_show")
   */

  public function show()
  {

    return $this->render(
      'inscription_show.html.twig',
      []
    );
  }

}
