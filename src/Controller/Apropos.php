<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class Apropos extends AbstractController
{

  /**
   * @Route("/apropos", name="apropos")
   */

  public function index()
  {
    return $this->render("apropos.html.twig", [
      ]);
  }

}
