<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Article;
use App\Repository\Articlepdo;

class Articleview extends AbstractController
{

  /**
   * @Route("/articleview", name="articleview")
   */

   

  public function index(Articlepdo $repo)
  {

    $article = $repo->getAll();
    
    return $this->render("articleview.html.twig", [
      'article' => $article,
      ]);
  }

}
