<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;

class Admin implements UserInterface
{

  public $id;
  public $mail;

  /**
   * @Assert\Length(min=5)
   */
  public $password;

  public function __construct( string $mail = null, string $password,int $id = null)
  {

    $this->id = $id;
    $this->mail = $mail;
    $this->password = $password;

  }

  public static function fromSQL(array $rawData)
  {
    return new Admin(
      $rawData["mail"],
      $rawData["password"],
      $rawData["id"]
    );
  }

  public function getPassword()
    {
        return $this->password;
    }

    public function getRoles()
    {
        return ["ROLE_USER"];
    }

    public function getSalt()
    {
        
    }

    public function getUsername()
    {
        return $this->mail;
    }

    public function eraseCredentials()
    {
        
    }

}
