<?php

namespace App\Entity;


class Videomp{
  public $author;
  public $title;
  public $link;
  public $date;
  public $hidden;
  public $tag;
  public $category;

  public function fromSQL(array $sql){
    $this->id = $sql["id"];
    $this->author = $sql["author"];
    $this->title = $sql["title"];
    $this->link = $sql["link"];
    $this->date = new \DateTime($sql["date"]);
    $this->hidden = $sql["hidden"];
    $this->tag = $sql["tag"];
    $this->category = $sql["category"];
    
  }
}