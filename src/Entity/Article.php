<?php

namespace App\Entity;


class Article {
  public $author;
  public $title;
  public $header;
  public $main;
  public $date;
  public $hidden;
  public $tag;
  public $category;

  public function fromSQL(array $sql){
    $this->id = $sql["id"];
    $this->author = $sql["author"];
    $this->title = $sql["title"];
    $this->header = $sql["header"];
    $this->main = $sql["main"];
    $this->date = new \DateTime($sql["date"]);
    $this->hidden = $sql["hidden"];
    $this->tag = $sql["tag"];
    $this->category = $sql["category"];
  }
}