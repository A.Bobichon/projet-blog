CREATE DATABASE  IF NOT EXISTS `db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db`;
-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: db
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.34-MariaDB-1~jessie

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Admin`
--

DROP TABLE IF EXISTS `Admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(45) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Admin`
--

LOCK TABLES `Admin` WRITE;
/*!40000 ALTER TABLE `Admin` DISABLE KEYS */;
INSERT INTO `Admin` VALUES (1,'patate@gmail','$2y$12$PU34xfINi9BXExCrL0L18OWo7oOGUYdfuL/Qnniv4jN.HPHe4vVWq');
/*!40000 ALTER TABLE `Admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Article`
--

DROP TABLE IF EXISTS `Article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `header` varchar(10000) DEFAULT NULL,
  `main` varchar(10000) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `hidden` int(11) DEFAULT NULL,
  `tag` varchar(45) DEFAULT NULL,
  `category` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Article`
--

LOCK TABLES `Article` WRITE;
/*!40000 ALTER TABLE `Article` DISABLE KEYS */;
INSERT INTO `Article` VALUES (4,'lafontaine','La Cigale et la Fourmi','La cigale ayant chantÃ©\r\nTout l\'Ã©tÃ©,\r\nSe trouva fort dÃ©pourvue\r\nQuand la bise fut venue.\r\nPas un seul petit morceau\r\nDe mouche ou de vermisseau.','Elle alla crier famine\r\nChez la Fourmi sa voisine,\r\nLa priant de lui prÃªter\r\nQuelque grain pour subsister\r\nJusqu\'Ã  la saison nouvelle.\r\nJe vous paierai, lui dit-elle,\r\nAvant lâ€™aoÃ»t, foi d\'animal,\r\nIntÃ©rÃªt et principal.\r\nLa Fourmi n\'est pas prÃªteuse,\r\nC\'est lÃ  son moindre dÃ©faut.\r\nQue faisiez-vous au temps chaud ?\r\nDit-elle Ã  cette emprunteuse.\r\nNuit et jour Ã  tout venant,\r\nJe chantais, ne vous dÃ©plaise.\r\nVous chantiez ? j\'en suis fort aise,\r\nEh bien! dansez maintenant.','2018-07-22',NULL,'fable','poesie'),(5,'lafontaine','Le Corbeau et le Renard','MaÃ®tre Corbeau sur un arbre perchÃ©,\r\nTenait en son bec un fromage.\r\nMaÃ®tre Renard par lâ€™odeur allÃ©chÃ©\r\nLui tint Ã  peu prÃ¨s ce langage :\r\nEt bonjour, Monsieur du Corbeau.','Que vous Ãªtes joli ! que vous me semblez beau !\r\nSans mentir, si votre ramage\r\nSe rapporte Ã  votre plumage,\r\nVous Ãªtes le Phenix des hÃ´tes de ces bois.\r\nÃ€ ces mots le Corbeau ne se sent pas de joie :\r\nEt pour montrer sa belle voix,\r\nIl ouvre un large bec, laisse tomber sa proie.\r\nLe Renard sâ€™en saisit, et dit : Mon bon Monsieur,\r\nApprenez que tout flatteur\r\nVit aux dÃ©pens de celui qui lâ€™Ã©coute.\r\nCette leÃ§on vaut bien un fromage sans doute.\r\nLe Corbeau honteux et confus\r\nJura, mais un peu tard, quâ€™on ne lâ€™y prendrait plus.','2018-07-22',NULL,'fable','poesie'),(6,'lafontaine','Le Berger et la Mer','Du rapport dâ€™un troupeau, dont il vivait sans soins\r\nSe contenta longtemps un voisin dâ€™Amphitrite.\r\nSi sa fortune Ã©tait petite,\r\nElle Ã©tait sÃ»re tout au moins.','Ã€ la fin les trÃ©sors dÃ©chargÃ©s sur la plage,\r\nLe tentÃ¨rent si bien quâ€™il vendit son troupeau,\r\nTrafiqua de lâ€™argent, le mit entier sur lâ€™eau\r\nCet argent pÃ©rit par naufrage.\r\nSon maÃ®tre fut rÃ©duit Ã  garder les Brebis\r\nNon plus Berger en chef comme il Ã©tait jadis,\r\nQuand ses propres Moutons paissaient sur le rivage\r\nCelui qui sâ€™Ã©tait vu Coridon ou Tircis,\r\nFut Pierrot et rien davantage.\r\nAu bout de quelque temps il fit quelques profits\r\nRacheta des bÃªtes Ã  laine\r\nEt comme un jour les vents retenant leur haleine,\r\nLaissaient paisiblement aborder les vaisseaux\r\nVous voulez de lâ€™argent, Ã´ Mesdames les Eaux,\r\nDit-il, adressez-vous, je vous prie, Ã  quelquâ€™autre :\r\nMa foi vous nâ€™aurez pas le nÃ´tre.\r\n\r\nCeci nâ€™est pas un conte Ã  plaisir inventÃ©.\r\nJe me sers de la vÃ©ritÃ©\r\nPour montrer par expÃ©rience,\r\nQuâ€™un sou quand il est assurÃ©,\r\nVaut mieux que cinq en espÃ©rance :\r\nQuâ€™il se faut contenter de sa condition\r\nQuâ€™aux conseils de la Mer et de lâ€™Ambition\r\nNous devons fermer les oreilles.\r\nPour un qui sâ€™en louera, dix mille sâ€™en plaindront.\r\nLa Mer promet monts et merveilles\r\nFiez-vous-y, les vents et les voleurs viendront.','2018-07-22',NULL,'fable','poesie'),(7,'lafontaine','Le Meunier, son Fils et lâ€™Ã‚ne','Lâ€™Invention des Arts Ã©tant un droit dâ€™aÃ®nesse,\r\nNous devons lâ€™Apologue Ã  lâ€™ancienne GrÃ¨ce.\r\nMais ce champ ne se peut tellement moissonner,\r\nQue les derniers venus nâ€™y trouvent Ã  glaner.','La feinte est un pays plein de terres dÃ©sertes.\r\nTous les jours nos Auteurs y font des dÃ©couvertes.\r\nJe tâ€™en veux dire un trait assez bien inventÃ©.\r\nAutrefois Ã  Racan Malherbe lâ€™a contÃ©.\r\nCes deux rivaux dâ€™Horace, hÃ©ritiers de sa Lyre,\r\nDisciples dâ€™Apollon, nos MaÃ®tres pour mieux dire,\r\nSe rencontrant un jour tout seuls et sans tÃ©moins ;\r\n(Comme ils se confiaient leurs pensers et leurs soins)\r\nRacan commence ainsi : Dites-moi, jevous prie,\r\nVous qui devez savoir les choses de la vie,\r\nQui par tous ses degrÃ©s avez dÃ©jÃ  passÃ©,\r\nEt que rien ne doit fuir en cet Ã¢ge avancÃ© ;\r\nÃ€ quoi me rÃ©soudrai-je ? Il est temps que jâ€™y pense.\r\nVous connaissez mon bien, mon talent, ma naissance.\r\nDois-je dans la Province Ã©tablir mon sÃ©jour ?\r\nPrendre emploi dans lâ€™ArmÃ©e ? ou bien charge Ã  la Cour ?\r\nTout au monde est mÃªlÃ© dâ€™amertume et de charmes.\r\nLa guerre a ses douceurs, lâ€™Hymen a ses alarmes.\r\nSi je suivais mon goÃ»t, je saurais oÃ¹ buter ;\r\nMais jâ€™ai les miens, la Cour, le peuple Ã  contenter.\r\nMalherbe lÃ -dessus. Contenter tout le monde !\r\nÃ‰coutez ce rÃ©cit avant que je rÃ©ponde.\r\n\r\nJâ€™ai lu dans quelque endroit, quâ€™un Meunier et son fils,\r\nLâ€™un vieillard, lâ€™autre enfant, non pas des plus petits,\r\nMais garÃ§on de quinze ans, si jâ€™ai bonne mÃ©moire,\r\nAllaient vendre leur Ã‚ne un certain jour de foire.\r\nAfin quâ€™il fÃ»t plus frais et de meilleur dÃ©bit,\r\nOn lui lia les pieds, on vous le suspendit ;\r\nPuis cet homme et son fils le portent comme un lustre ;\r\nPauvres gens, idiots, couple ignorant et rustre.\r\nLe premier qui les vit, de rire sâ€™Ã©clata.\r\nQuelle farce, dit-il, vont joÃ¼er ces gens-lÃ  ?\r\nLe plus Ã¢ne des trois nâ€™est pas celui quâ€™on pense.\r\nLe Meunier Ã  ces mots connaÃ®t son ignorance.\r\nIl met sur pieds sa bÃªte, et la fait dÃ©taler.\r\nLâ€™Ã‚ne, qui goÃ»tait fort lâ€™autre faÃ§on dâ€™aller\r\nSe plaint en son patois. Le Meunier nâ€™en a cure.\r\nIl fait monter son fils, il suit, et dâ€™aventure\r\nPassent trois bons Marchands. Cet objet leur dÃ©plut.\r\nLe plus vieux au garÃ§on sâ€™Ã©cria tant quâ€™il put :\r\nOh lÃ  oh, descendez, que lâ€™on ne vous le dise,\r\nJeune homme qui menez Laquais Ã  barbe grise.\r\nCâ€™Ã©tait Ã  vous de suivre, au vieillard de monter.\r\nMessieurs, dit le Meunier, il vous faut contenter.\r\nLâ€™enfant met pied Ã  terre, et puis le vieillard monte ;\r\nQuand trois filles passant, lâ€™une dit : Câ€™est grandâ€™ honte,\r\nQuâ€™il faille voir ainsi clocher ce jeune fils ;\r\nTandis que ce nigaud, comme un Ã‰vÃªque assis,\r\nFait le veau sur son Ã‚ne, et pense Ãªtre bien sage.\r\nIl nâ€™est, dit le Meunier, plus de Veaux Ã  mon Ã¢ge.\r\nPassez votre chemin, la fille, et mâ€™en croyez.\r\nAprÃ¨s maints quolibets coup sur coup renvoyÃ©s,\r\nLâ€™homme crut avoir tort, et mit son fils en croupe.\r\nAu bout de trente pas une troisiÃ¨me troupe\r\nTrouve encore Ã  gloser. Lâ€™un dit : Ces gens sont fous,\r\nLe Baudet nâ€™en peut plus, il mourra sous leurs coups.\r\nHÃ© quoi, charger ainsi cette pauvre Bourrique !\r\nNâ€™ont-ils point de pitiÃ© de leur vieux domestique ?\r\nSans doute quâ€™Ã  la Foire ils vont vendre sa peau.\r\nParbieu, dit le Meunier, est bien fou du cerveau,\r\nQui prÃ©tend contenter tout le monde et son pÃ¨re.\r\nEssayons toutefois, si par quelque maniÃ¨re\r\nNous en viendrons Ã  bout. Ils descendent tous deux.\r\nLâ€™Ã‚ne se prÃ©lassant marche seul devant eux.\r\nUn quidam les rencontre, et dit : Est-ce la mode,\r\nQue Baudet aille Ã  lâ€™aise, et Meunier sâ€™incommode ?\r\nQui de lâ€™Ã‚ne ou du MaÃ®tre est fait pour se laisser ?\r\nJe conseille Ã  ces gens de le faire enchÃ¢sser.\r\nIls usent leurs souliers, et conservent leur Ã‚ne :\r\nNicolas au rebours ; car quand il va voir Jeanne,\r\nIl monte sur sa bÃªte, et la chanson le dit.\r\nBeau trio de Baudets ! le Meunier repartit :\r\nJe suis Ã‚ne, il est vrai, jâ€™en conviens, je lâ€™avoue ;\r\nMais que dorÃ©navant on me blÃ¢me, on me loue ;\r\nQuâ€™on dise quelque chose, ou quâ€™on ne dise rien ;\r\nJâ€™en veux faire Ã  ma tÃªte. Il le fit, et fit bien.\r\n\r\nQuant Ã  vous, suivez Mars, ou lâ€™Amour, ou le Prince ;\r\nAllez, venez, courez, demeurez en Province ;\r\nPrenez femme, Abbaye, Emploi, Gouvernement ;\r\nLes gens en parleront, nâ€™en doutez nullement.','2018-07-23',NULL,'fable','poesie'),(8,'Eric-Emmanuel Schmitt','Le secret de la cognition : de Schmitt','SpontanÃ©ment, les liens qui rÃ©unissent Eric-Emmanuel Schmitt et la cognition ne semblent pas Ã©vidents. Pourtant, quand le premier Ã©crit Madame Pylinska et le secret de Chopin et que dâ€™autres pensent les sciences cognitives, tous se focalisent sur lâ€™esprit humain. Un Ã©clairage sans doute utile pour la philosophie.','Ce conte nâ€™est pas anodin au sens oÃ¹ il met le lecteur directement en face de sa condition humaine : les choix parfois surprenants du narrateur, ou encore les pensÃ©es irrationnelles quâ€™il peut avoir sont notre lot quotidien. En cela, dÃ©jÃ , lâ€™Å“uvre vient nous questionner : quâ€™aurions-nous fait face Ã  une professeure de piano aux demandes excentriques ou encore confrontÃ© Ã  la maladie dâ€™un proche ?\r\nLe piano, lui, est tel un animal de compagnie, dont la prÃ©sence est au dÃ©but encombrante, notamment du fait de lâ€™hÃ©ritage quâ€™il reprÃ©sente, pour peu Ã  peu devenir un compagnon de route et de vie. Plus que le piano en lui-mÃªme, Chopin compte encore plus pour Schmitt, telle une force de rÃ©sistance de lâ€™enfance dans sa vie de jeune adulte.\r\n\r\nLâ€™intrigue est donc profondÃ©ment personnelle ; mais endosse trÃ¨s rÃ©guliÃ¨rement une facette philosophique. La place du silence dans la musique semble sâ€™imposer Ã©galement Ã  la lecture, dont le moindre bruit pourrait perturber la transe.\r\n\r\nOu encore lorsque Madame Pylinska implore le narrateur Ã  devenir liquide : Â« Liquideâ€¦ CÃ©der Ã  lâ€™onde, saisir lâ€™espace entre les sons sans lâ€™agripper, se livrer Ã  ce qui arrive, Ã©largir sa disponibilitÃ©. Liquideâ€¦ Â» (page 39) ; non sans rappeler Bauman et Lâ€™Amour liquide. Dâ€™ailleurs, lâ€™enseignante assÃ¨ne au sujet de lâ€™amour quâ€™ Â« on nâ€™aime vraiment que lorsquâ€™on nâ€™est pas amoureux Â» Lâ€™ouvrage est aussi une leÃ§on magistrale autour de Chopin. Lâ€™on y apprend quâ€™il Â« ne part pas de quoi que ce soit dâ€™antÃ©rieur : il crÃ©e ! Aucune image mentale ne prÃ©existe Ã  sa musique. Câ€™est la musique qui impose sa rÃ©alitÃ© Ã  lâ€™esprit. Elle demeure pure. Elle nâ€™exprime pas de sentiments, elle les provoque Â» (page 49).\r\n\r\nSans oublier lâ€™importance pour Schmitt de lâ€™intÃ©rioritÃ© et de lâ€™intime, qui affirme par la voix de sa tante Â« une mauvaise rÃ©putation fournit une armure souveraine Ã  quiconque prÃ©tend rester discret Â» (page 56). Le lecteur apprend alors que câ€™est grÃ¢ce Ã  Madame Pylinska que non seulement lâ€™auteur Ã©crit mais aussi quâ€™il consacre une telle place Ã  lâ€™intimitÃ©. Peu Ã  peu, et notamment dans la veine de La nuit de feu, Schmitt se livre Ã  ses lecteurs, et nous offre chaque fois un peu plus une vue nouvelle sur sa vie, quitte Ã  nous emporter dans un vertige philosophique dont il a le secret.\r\nLa cognition, du neurone Ã  la sociÃ©tÃ©, dirigÃ© par T. Collins, D. Andler et C. Tallon-Baudry\r\n\r\nLes sciences cognitives Â« regroupent les disciplines qui placent lâ€™esprit humain au cÅ“ur de leurs interrogations Â», telles que Â« la biologie, la psychologie, la philosophie, la linguistique, lâ€™anthropologie et lâ€™intelligence artificielle Â» (page 10).\r\n\r\nLâ€™ouvrage offre une perspective croissante en termes dâ€™Ã©chelles : molÃ©culaire, neuronale, corporelle, socialeâ€¦ Surtout, la transdisciplinaritÃ© permise par les sciences cognitives donne un sens nouveau Ã  lâ€™action, au langage, Ã  la perception, aux Ã©motions ou encore au dÃ©veloppement.\r\n\r\nA la fois guide et initiation, lâ€™ouvrage dÃ©cloisonne enfin les disciplines et appelle Ã  une pensÃ©e plus globale et complexe, dont la comprÃ©hension de lâ€™activitÃ© humaine a pleinement besoin.\r\n\r\nGuillaume Plaisance\r\nBibliographie\r\n\r\nSchmitt, Eric-Emmanuel â€“ Madame Pylinska et le secret de Chopin â€“ Albin Michel â€“ 2018\r\nCollins, ThÃ©rÃ¨se ; Andler, Daniel et Tallon-Baudry Catherin â€“ La cognition â€“ Folio â€“ 2018','2018-07-23',NULL,'litterature','Philosophie');
/*!40000 ALTER TABLE `Article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Video`
--

DROP TABLE IF EXISTS `Video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `link` varchar(10000) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `hidden` varchar(45) DEFAULT NULL,
  `tag` varchar(45) DEFAULT NULL,
  `category` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Video`
--

LOCK TABLES `Video` WRITE;
/*!40000 ALTER TABLE `Video` DISABLE KEYS */;
INSERT INTO `Video` VALUES (4,'Dissidence','Dissidence Coalition','https://www.youtube.com/embed/BH8JLbCalh4','2018-07-22',NULL,'trottinette','sport'),(5,'Dissidence','Dissidence Coalition','https://www.youtube.com/embed/Wh6CqyDwhYY','2018-07-22',NULL,'trottinette','sport'),(6,'Stevie churchill','Oss edit bmx','https://www.youtube.com/embed/jYrzNpwhUk4','2018-07-22',NULL,'bmx','sport'),(7,'Brendon begins','Oss edit bmx','https://www.youtube.com/embed/AjVAemENLXY','2018-07-22',NULL,'bmx','sport'),(8,'Brendon begins & Stevie churchill','Skatepark edit brendon X stevie','https://www.youtube.com/embed/TVXOvRIC4q8','2018-07-22',NULL,'bmx','sport');
/*!40000 ALTER TABLE `Video` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-26 14:19:51
